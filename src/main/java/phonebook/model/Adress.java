package phonebook.model;

import lombok.Data;

@Data
public class Adress {

    private String id;
    private String adress;

    public Adress() {
    }

    public Adress(String id, String adress) {
        this.id = id;
        this.adress = adress;
    }
}
