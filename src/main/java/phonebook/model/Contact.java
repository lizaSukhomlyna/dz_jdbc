package phonebook.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Contact {

    private String id;
    private String name;
    private String surName;

}
