package phonebook.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import phonebook.manager.AdressService;
import phonebook.manager.ContactService;
import phonebook.manager.DbManager;
import phonebook.model.Adress;
import phonebook.model.Contact;

import java.sql.Statement;

@RestController
@RequestMapping(path = "/api/contact")
public class ContactBookController {

    @Autowired
    private DbManager dbManager;

    @Autowired
    private AdressService adressService;

    @Autowired
    private ContactService contactService;

    @PostMapping("/openConnection")
    public String openConnection() {
        Statement statement = dbManager.openConnection();
        adressService.setStatement(statement);
        contactService.setStatement(statement);
        return "Connection is open";
    }

    @PostMapping("/closeConnection")
    public String closeConnection() {
        dbManager.closeConnection();
        return "Connection is close";
    }

    @GetMapping("/getContact")
    public String getContact(@RequestParam String id) {

        return contactService.selectContactsById(id);
    }

    @GetMapping("/getAdress")
    public String getAdress(@RequestParam String id) {
        return adressService.selectAdressById(id);
    }

    @PostMapping("/addContact")
    public String addContact(@RequestBody Contact contact) {
        return contactService.insertRecordToContacts(contact);
    }

    @PostMapping("/addAdress")
    public String addAdress(@RequestBody Adress adress) {
        return adressService.insertRecordToAdress(adress);
    }

    @PostMapping("/removeContact")
    public String removeContactById(@RequestParam String id) {
        return contactService.removeRecordById(id);
    }

    @PostMapping("/removeAdress")
    public String removeAdressById(@RequestParam String id) {
        return adressService.removeAdressById(id);
    }

    @PostMapping("/updateAdress")
    public String addAdress(@RequestParam String id, @RequestParam String updateAdress) {
        return adressService.updateAdressById(id, updateAdress);
    }
}
