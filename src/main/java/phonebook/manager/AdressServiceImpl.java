package phonebook.manager;

import org.springframework.stereotype.Service;
import phonebook.model.Adress;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class AdressServiceImpl implements AdressService {

    Statement statement;

    @Override
    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public String selectAdressById(String id) {
        try {
            String sql = "SELECT * FROM PHONE_BOOK.adress" +
                    " WHERE id='" + id + "';";
            ResultSet resultSet = statement.executeQuery(sql);
            Adress adress = new Adress();
            while (resultSet.next()) {
                adress.setId(resultSet.getString("id"));
                adress.setAdress(resultSet.getString("adress"));
            }
            return adress.toString();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public String insertRecordToAdress(Adress adress) {
        try {
            String sql = "INSERT INTO PHONE_BOOK.adress(id, adress) VALUES ('"
                    + adress.getId() + "', '" + adress.getAdress() + "');";
            statement.execute(sql);
            return "Adress with id:" + adress.getId() + " was added";
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public String removeAdressById(String id) {
        try {
            String sql = "DELETE FROM PHONE_BOOK.adress" +
                    " WHERE id='" + id + "';";
            statement.execute(sql);
            return "Adress with id: " + id + " was deleted";
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    @Override
    public String updateAdressById(String id, String adress) {
        try {
            String sql = "UPDATE PHONE_BOOK.adress" +
                    " SET adress='" + adress +
                    "' WHERE id='" + id + "';";
            statement.execute(sql);

            return "Adress with id: " + id + " was updated";
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

}
