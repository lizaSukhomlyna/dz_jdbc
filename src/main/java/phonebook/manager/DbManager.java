package phonebook.manager;

import java.sql.Statement;

public interface DbManager {
    Statement openConnection();

    void closeConnection();
}
