package phonebook.manager;

import phonebook.model.Adress;

import java.sql.Statement;

public interface AdressService {

    void setStatement(Statement statement);

    String selectAdressById(String id);

    String removeAdressById(String id);

    String updateAdressById(String id, String adress);

    String insertRecordToAdress(Adress adress);
}
