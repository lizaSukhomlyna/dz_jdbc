package phonebook.manager;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


@Service
public class DbManagerMySQL implements DbManager {
    Statement statement;

    @Value("${database.url}")
    private String DATABASE_URL;

    @Value("${database.user}")
    private String USER;
    @Value("${database.password}")
    private String PASSWORD;

    Connection connection;


    @Override
    public Statement openConnection() {
        try {
            connection = DriverManager.getConnection(DATABASE_URL, USER, PASSWORD);
            statement = connection.createStatement();
            return statement;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public void closeConnection() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }
}
