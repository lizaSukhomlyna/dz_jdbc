package phonebook.manager;

import org.springframework.stereotype.Service;
import phonebook.model.Contact;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class ContactServiceImpl implements ContactService {
    Statement statement;

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    @Override
    public String insertRecordToContacts(Contact contact) {
        try {
            String sql = "INSERT INTO PHONE_BOOK.contacts(id, name, surname) VALUES ('"
                    + contact.getId() + "', '" + contact.getName() + "','" + contact.getSurName() + "');";
            statement.execute(sql);
            return contact.getName() + " was added";
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public String selectContactsById(String id) {
        try {
            String sql = "SELECT * FROM PHONE_BOOK.contacts" +
                    " WHERE id='" + id + "';";
            ResultSet resultSet = statement.executeQuery(sql);
            Contact contact = new Contact();
            while (resultSet.next()) {
                contact.setId(resultSet.getString("id"));
                contact.setName(resultSet.getString("name"));
                contact.setSurName(resultSet.getString("surname"));
            }
            return contact.toString();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }

    @Override
    public String removeRecordById(String id) {
        try {
            String sql = "DELETE FROM PHONE_BOOK.contacts" +
                    " WHERE id='" + id + "';";
            statement.execute(sql);
            return "Contact with id: " + id + " was deleted";
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
