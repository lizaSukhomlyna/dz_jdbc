package phonebook.manager;

import phonebook.model.Contact;

import java.sql.Statement;

public interface ContactService {
    void setStatement(Statement statement);

    String selectContactsById(String id);

    String insertRecordToContacts(Contact contact);

    String removeRecordById(String id);
}
